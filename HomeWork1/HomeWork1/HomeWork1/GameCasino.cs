﻿using System;

namespace HomeWork1
{
    class GameCasino
    {
        public GameCasino(string input)
        {
            string[] inputArray = input.Split(' ');
            Date             = DataBaseUtils.ConvertStringToDate(inputArray[0]);
            NameUser         = inputArray[1];
            GameCode         = Convert.ToInt32(inputArray[2]);
            Status           = Convert.ToInt32(inputArray[3]);
            BalanceChange    = Convert.ToInt32(inputArray[4]);

            SummGamer        = inputArray[5];
            SummCasino       = Convert.ToInt32(inputArray[6]);
        }
        public GameCasino(string name, int id)
        {
            Date = DateTime.UtcNow;
            NameUser = name;
            GameCode = id;
        }

        public override string ToString()
        {
            string result = "Data:" + Date + "." + " Player:" + NameUser + "." + " Game:";
            switch (GameCode)
            {
                case 0:
                    result += "BJ.";
                    break;
                case 1:
                    result += "Roulet.";
                    break;
                case 2:
                    result += "Dice.";
                    break;
            }
            
            result += " You:";
            if (GameCode == 1 && ((SummGamer.Contains("r") || SummGamer.Contains("b"))))
            {
                result += SummGamer + ".";
                result += " Winning:";
                if (SummCasino % 2 == 0)
                {
                    result += "r.";
                }
                else
                {
                    result += "b.";
                }
            }
            else
            {
                result += SummGamer + "." + " Winning:" + SummCasino + ".";
            }

            if (Status == 0)
            {
                result += " Win:" + BalanceChange + ".";
            }
            else
            {
                result += " Loose:" + BalanceChange + ".";
            }

            return result;
        }


        public DateTime Date          { get; set; }
        public string   NameUser      { get; set; }
        public int      GameCode      { get; set; }
        public int      Status        { get; set; }
        public int      BalanceChange { get; set; }

        public int      Bet           { get; set; }
        public int      BalansEndGame { get; set; }
        public string   SummGamer     { get; set; }
        public int      SummCasino    { get; set; }
        
    }
}
