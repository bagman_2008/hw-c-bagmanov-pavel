﻿using System;

namespace HomeWork1
{
    class CasinoUtils
    {
        public static int    CardToPoints(int x)
        {
            int j = 2;
            for (int i = 1; i <= 33; i += 4)
            {
                if (i <= x && x <= i + 3)
                {
                    return j;
                }
                else
                {
                    ++j;
                }
            }

            j = 2;
            for (int i = 37; i <= 45; i += 4)
            {
                if (i <= x && x <= i + 3)
                {
                    return j;
                }
                else
                {
                    ++j;
                }
            }

            return 1;
        }
        public static int    ReadingNumberRange(int l, int r)
        {
            int x;
            while (true)
            {
                string buffer = Console.ReadLine();
                try
                {
                    x = Convert.ToInt32(buffer);
                }
                catch (FormatException)
                {
                    Console.WriteLine("Enter a number from " + l + " to " + r + " and press <Enter>");
                    continue;
                }
                catch (ArgumentNullException)
                {
                    Console.WriteLine("Enter a number from " + l + " to " + r + " and press <Enter>");
                    continue;
                }
                catch (OverflowException)
                {
                    Console.WriteLine("Enter a number from " + l + " to " + r + " and press <Enter>");
                    continue;
                }

                if (x >= l && x <= r)
                {
                    break;
                }
                else
                {
                    Console.WriteLine("Enter a number from " + l + " to " + r + " and press <Enter>");
                }
            }

            return x;
        }
        public static int[]  GetPackPlayingCards()
        {
            int[] cart = new int[53];
            Random rand = new Random();
            for (int i = 1; i <= 52; ++i)
            {
                cart[i] = i;
            }
            for (int i = 0; i <= 300; ++i)
            {
                int x = rand.Next(1, 52);
                int y = rand.Next(1, 52);
                int z = cart[x];
                cart[x] = cart[y];
                cart[y] = z;
            }
            return cart;
        }
        public static string GetString()
        {
            string name;
            while (true)
            {
                name = Console.ReadLine();
                if (String.IsNullOrEmpty(name))
                {
                    Console.Write("You entered an incorrect name, try again: ");
                }
                else
                {
                    int t = 0;
                    for (int i = 0; i < name.Length; ++i)
                    {
                        if (Char.IsLetter(name[i]) == false)
                        {
                            t = 1;
                        }
                    }

                    if (t == 1)
                    {
                        Console.WriteLine("You entered an incorrect name, try again: ");
                    }
                    else
                    {
                        break;
                    }
                }
            }
            return name;
        }

        public static void PrintMenuGame()
        {
            Console.WriteLine("Select a game:");
            Console.WriteLine("1 - Roulette.");
            Console.WriteLine("2 - Dice.");
            Console.WriteLine("3 - BlackJack.");
            Console.WriteLine("4 - Displaying the results of games.");
            Console.WriteLine("5 - Deposit money into the account.");
            Console.WriteLine("6 - Exit");
        }
        public static void PrintContinueGame()
        {
            Console.WriteLine("Continue the game?");
            Console.WriteLine("1 - Yes");
            Console.WriteLine("2 - No");
        }
    }
}
