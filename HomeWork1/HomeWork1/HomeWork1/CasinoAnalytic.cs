﻿using System;
using System.Collections.Generic;
using System.Linq;
using CasinoAnalysis;

namespace HomeWork1
{
    class CasinoAnalytic:ICasinoAnalytic
    {
        CasinoAnalytic(string nameLogFile)
        {
            DataBase logDataBase = new DataBase(nameLogFile);

            _bets          = logDataBase.GetAllBets();
            _deposites     = logDataBase.GetAllDeposites();
            _gameResultses = logDataBase.GetAllGame();
            _users         = logDataBase.GetAllUser();
        }

        public IEnumerable<Bet> Bets
        {
            get
            {
                return _bets;
            }
        }

        public IEnumerable<Deposite> Deposites
        {
            get
            {
                return _deposites;
            }
        }

        public IEnumerable<GameResults> GamesResults
        {
            get
            {
                return _gameResultses;
            }          
        }


        public Bet MaxBet()
        {
            int max = _bets.Max(bet=>bet.Value);
            return _bets.First(bet => bet.Value == max);
        }
        public Bet MaxBet(GameCode code)
        {
            int max = _bets.Where(bet => bet.GameCode == code).Max(bet => bet.Value);
            return _bets.First(bet => bet.Value == max && bet.GameCode == code);
        }

        public int AverageDeposite()
        {
            int summ = 0;
            int count = 0;
            foreach (var element in _deposites)
            {
                summ += element.Value;
                count++;
            }
            if (count == 0)
            {
                return 0;
            }
            return summ/count;
        }
        public int AverageDeposite(User user)
        {
            int summ = 0;
            int count = 0;
            foreach (var element in _deposites)
            {
                if (element.User.Name == user.Name)
                {
                    summ += element.Value;
                    count++;
                }
            }
            if (count == 0)
            {
                return 0;
            }
            return summ / count;
        }

        public IEnumerable<Deposite> TopDeposites(int count)
        {
            Dictionary<string, Deposite> dictonaryDeposites = new Dictionary<string, Deposite>();

            foreach (var deposit in _deposites)
            {
                if (dictonaryDeposites.ContainsKey(deposit.User.Name))
                {
                    if (dictonaryDeposites[deposit.User.Name].Date < deposit.Date)
                    {
                        dictonaryDeposites[deposit.User.Name] = deposit;
                    }
                }
                else
                {
                    dictonaryDeposites.Add(deposit.User.Name, deposit);
                }
            }
            return dictonaryDeposites.Values.ToList().OrderByDescending(deposite => deposite.Value).Take(count);
        }

        public IEnumerable<Deposite> RichestClients(int count)
        {
            Dictionary<string, int> dictonaryGameResultses = new Dictionary<string, int>();
            foreach (var game in _gameResultses)
            {
                int val = game.Status == GameResultStatus.Win ? game.BalanceChange : (-1) * game.BalanceChange;
                if (dictonaryGameResultses.ContainsKey(game.User.Name))
                {
                    dictonaryGameResultses[game.User.Name] += val;
                }
                else
                {
                    dictonaryGameResultses.Add(game.User.Name, val);
                }
            }

            Dictionary<string, Deposite> dictonaryDeposites = new Dictionary<string, Deposite>();

            foreach (var deposit in _deposites)
            {
                if (dictonaryDeposites.ContainsKey(deposit.User.Name))
                {
                    if (dictonaryDeposites[deposit.User.Name].Date < deposit.Date)
                    {
                        dictonaryDeposites[deposit.User.Name] = deposit;
                    }
                }
                else
                {
                    dictonaryDeposites.Add(deposit.User.Name, deposit);
                }
            }

            List<Deposite> answer = new List<Deposite>(dictonaryDeposites.Values);
            return answer.OrderByDescending(deposite => deposite.Value + (-1)*dictonaryGameResultses[deposite.User.Name])
                .Take(count);

        }

        public GameCode MaxProfitGame(out int amount)
        {
            Dictionary<GameCode, int> dictonaryGameSumm = new Dictionary<GameCode, int>();
            foreach (var game in _gameResultses)
            {
                int val = game.Status == GameResultStatus.Loose ? game.BalanceChange : (-1) * game.BalanceChange;
                if (dictonaryGameSumm.ContainsKey(game.GameCode))
                {
                    dictonaryGameSumm[game.GameCode] += val;
                }
                else
                {
                    dictonaryGameSumm.Add(game.GameCode, val);
                }
            }
            GameCode s = new GameCode();
            amount = Int32.MinValue;
            foreach (var game in dictonaryGameSumm)
            {
                if (amount < game.Value)
                {
                    s = game.Key;
                    amount = game.Value;
                }
            }
            if (amount == Int32.MinValue)
            {
                amount = 0;
            }
            return s;

        }

        public User MostLuckyUser(GameCode game)
        {
            Dictionary<string, WinLooseCounter> dictonaryGameSumm = new Dictionary<string, WinLooseCounter>();

            foreach (var games in _gameResultses)
            {
                if (games.GameCode != game)
                {
                    continue;
                }
                if (dictonaryGameSumm.ContainsKey(games.User.Name))
                {
                    if (games.Status == GameResultStatus.Win)
                    {
                        dictonaryGameSumm[games.User.Name].Win += 1;
                    }
                    else
                    {
                        dictonaryGameSumm[games.User.Name].Loose += 1;
                    }
                }
                else
                {
                    dictonaryGameSumm.Add(games.User.Name, new WinLooseCounter());
                    if (games.Status == GameResultStatus.Win)
                    {
                        dictonaryGameSumm[games.User.Name].Win += 1;
                    }
                    else
                    {
                        dictonaryGameSumm[games.User.Name].Loose += 1;
                    }
                }
            }
            double max = Double.MinValue;
            string stringUser = "a";
            foreach (var games in dictonaryGameSumm)
            {
                if (max < (double)(games.Value.Win) /(games.Value.Loose))
                {
                    max = (double) (games.Value.Win)/(games.Value.Loose);
                    stringUser = games.Key;
                }
            }

            return _users[stringUser];
        }

        public User MaxLuckyUser()
        {
            Dictionary<string, WinLooseCounter> dictonaryGameSumm = new Dictionary<string, WinLooseCounter>();

            foreach (var games in _gameResultses)
            {
                if (dictonaryGameSumm.ContainsKey(games.User.Name))
                {
                    if (games.Status == GameResultStatus.Win)
                    {
                        dictonaryGameSumm[games.User.Name].Win += 1;
                    }
                    else
                    {
                        dictonaryGameSumm[games.User.Name].Loose += 1;
                    }
                }
                else
                {
                    dictonaryGameSumm.Add(games.User.Name, new WinLooseCounter());
                    if (games.Status == GameResultStatus.Win)
                    {
                        dictonaryGameSumm[games.User.Name].Win += 1;
                    }
                    else
                    {
                        dictonaryGameSumm[games.User.Name].Loose += 1;
                    }
                }
            }
            double max = Double.MinValue;
            string stringUser = "a";
            foreach (var games in dictonaryGameSumm)
            {
                if (games.Value.Loose == 0)
                {
                    return null;
                }
                if (max < (double)(games.Value.Win) /(games.Value.Loose))
                {
                    max = (double)(games.Value.Win) /(games.Value.Loose);
                    stringUser = games.Key;
                }
            }

            return _users[stringUser];
        }

        public int UserDeposite(User user, DateTime date)
        {
            int answer = 0;
            DateTime currDate = new DateTime();
            foreach (var deposit in _deposites)
            {
                if (user.Name == deposit.User.Name && deposit.Date <= date && currDate < deposit.Date)
                {
                    answer = deposit.Value;
                    currDate = deposit.Date;
                }
            }
            return answer;
        }

        public IEnumerable<int> ZeroBasedBalanceHistoryExchange(User user)
        {
            return ZeroBasedBalanceHistoryExchange(user, DateTime.MinValue);
        }

        public IEnumerable<int> ZeroBasedBalanceHistoryExchange(User user, DateTime from)
        {
            List<int> answer = new List<int>();
            IEnumerable<GameResults> list = _gameResultses.Where(c => c.User.Name == user.Name && c.Date >= from).OrderBy(c => c.Date);
            foreach (var curr in list)
            {
                if (answer.Count == 0)
                {
                    answer.Add(curr.Status == GameResultStatus.Win ? curr.BalanceChange : (-1)*curr.BalanceChange);
                }
                else
                {
                    answer.Add( answer.Last() + curr.Status == GameResultStatus.Win ? curr.BalanceChange : (-1) * curr.BalanceChange);
                }
            }
            return answer;
        }

        public Dictionary<DateTime, int> GamesCountByMounths()
        {
            Dictionary<DateTime, int> answer = new Dictionary<DateTime, int>();
            foreach (var curr in _gameResultses)
            {
                DateTime s = new DateTime(curr.Date.Year, curr.Date.Month, DateTime.DaysInMonth(curr.Date.Year, curr.Date.Month),0,0,0,0);
                if (answer.ContainsKey(s))
                {
                    answer[s] += 1;
                }
                else
                {
                    answer.Add(s,1);
                }
            }
            return answer;
        }

        public Dictionary<DateTime, int> GamesCountByMounths(GameCode game)
        {
            Dictionary<DateTime, int> answer = new Dictionary<DateTime, int>();
            foreach (var curr in _gameResultses)
            {
                if (curr.GameCode != game)
                {
                    continue;
                }
                DateTime s = new DateTime(curr.Date.Year, curr.Date.Month, DateTime.DaysInMonth(curr.Date.Year, curr.Date.Month), 0, 0, 0, 0);
                if (answer.ContainsKey(s))
                {
                    answer[s] += 1;
                }
                else
                {
                    answer.Add(s, 1);
                }
            }
            return answer;
        }

        public Dictionary<DateTime, int> NewUsersByMounths()
        {
            Dictionary <string,DateTime> s= new Dictionary<string, DateTime>();
            foreach (var curr in _gameResultses)
            {
                if (s.ContainsKey(curr.User.Name))
                {
                    if (s[curr.User.Name] > curr.Date)
                    {
                        s[curr.User.Name] = curr.Date;
                    }
                }
                else
                {
                    s.Add(curr.User.Name, curr.Date);
                }
            }
            Dictionary <DateTime,int > answer = new Dictionary<DateTime, int>();
            foreach (var curr in s)
            {
                DateTime x = new DateTime(curr.Value.Year, curr.Value.Month, DateTime.DaysInMonth(curr.Value.Year, curr.Value.Month), 0, 0, 0, 0);
                if (answer.ContainsKey(x))
                {
                    answer[x] += 1;
                }
                else
                {
                    answer.Add(x, 1);
                }
            }
            return answer;
        }

        public Dictionary<DateTime, int> NewUsersByMounths(GameCode game)
        {
            Dictionary<string, DateTime> s = new Dictionary<string, DateTime>();
            foreach (var curr in _gameResultses)
            {
                if (curr.GameCode != game)
                {
                    continue;
                }
                if (s.ContainsKey(curr.User.Name))
                {
                    if (s[curr.User.Name] > curr.Date)
                    {
                        s[curr.User.Name] = curr.Date;
                    }
                }
                else
                {
                    s.Add(curr.User.Name, curr.Date);
                }
            }
            Dictionary<DateTime, int> answer = new Dictionary<DateTime, int>();
            foreach (var curr in s)
            {
                DateTime x = new DateTime(curr.Value.Year, curr.Value.Month, DateTime.DaysInMonth(curr.Value.Year, curr.Value.Month), 0, 0, 0, 0);
                if (answer.ContainsKey(x))
                {
                    answer[x] += 1;
                }
                else
                {
                    answer.Add(x, 1);
                }
            }
            return answer;
        }

        private List<Bet>                _bets;
        private List<Deposite>           _deposites;
        private List<GameResults>        _gameResultses;
        private Dictionary<string, User> _users;
    }

    class WinLooseCounter
    {
        public int Win;
        public int Loose;
    }
}
