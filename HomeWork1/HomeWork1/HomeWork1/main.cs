﻿using System;

namespace HomeWork1
{
    class MyMain
    {
        static void Main()
        {

            Console.WriteLine("           Hello!");
            Console.WriteLine("     Welcome to Casino:");
            Console.WriteLine("A great boon for small coin.");
            Console.Write("Enter your name: ");

            string name = CasinoUtils.GetString();

            Console.WriteLine("Your name:" + name);
            Casino muCasino = new Casino(name, "data.log");

            int x = 0;
            while (x != 6)
            {
                CasinoUtils.PrintMenuGame();
                x = CasinoUtils.ReadingNumberRange(1, 6);
                switch (x)
                {
                    case 1:
                        muCasino.Roulette();
                        break;
                    case 2:
                        muCasino.Dice();
                        break;
                    case 3:
                        muCasino.BlackJack();
                        break;
                    case 4:
                        muCasino.PrintLogs();
                        break;
                    case 5:
                        muCasino.NewBalanse();
                        break;
                }
            }
            Console.WriteLine("Bye!");
        }
    }

    class Casino
    {
        public Casino(string nameUser, string nameDataBase)
        {
            _nameGamer = nameUser;
            _logDataBase = new DataBase(nameDataBase);
            if (_logDataBase.SearchGamer(nameUser))
            {
                _balans = _logDataBase.GetBalanseGamer(nameUser);
            }
            else
            {
                _logDataBase.AddNewGamer(nameUser);
                _balans = 0;
            }
        }

        public void Roulette()
        {
            int choice = 1;
            while (choice == 1)
            {
                GameCasino saveGame = new GameCasino(_nameGamer, 1);
                Console.WriteLine("Make your bid from 0 to " + _balans + ":");
                saveGame.Bet = CasinoUtils.ReadingNumberRange(0, _balans);

                Console.WriteLine("You bet on the color or number?");
                Console.WriteLine("1 - color, 2 - number");
                int typeGame = CasinoUtils.ReadingNumberRange(1, 2);

                if (typeGame == 1)
                {
                    Console.WriteLine("1 - black, 2 - red");
                    saveGame.SummGamer = CasinoUtils.ReadingNumberRange(1, 2) == 1 ? "b" : "r";
                }
                else
                {
                    Console.WriteLine("Enter a number from 1 to 36");
                    saveGame.SummGamer = CasinoUtils.ReadingNumberRange(1, 36).ToString();
                }

                Random rand = new Random();
                saveGame.SummCasino = rand.Next(1, 36);

                if (typeGame == 2)
                {
                    if (Convert.ToInt32(saveGame.SummGamer) == saveGame.SummCasino)
                    {
                        Console.WriteLine("You won: " + saveGame.Bet * 3 + " $.");
                        _balans += 3 * saveGame.Bet;
                        saveGame.BalanceChange = 3*saveGame.Bet;
                        saveGame.Status = 0;
                    }
                    else
                    {
                        Console.WriteLine("You lost: " + saveGame.Bet + " $.");
                        _balans -= saveGame.Bet;
                        saveGame.BalanceChange = saveGame.Bet;
                        saveGame.Status = 1;
                    }
                }
                else
                {
                    string result = saveGame.SummCasino % 2 == 0 ? "r" : "b";
                    if (saveGame.SummGamer == result)
                    {
                        Console.WriteLine("You won: " + saveGame.Bet + " $.");
                        _balans += saveGame.Bet;
                        saveGame.BalanceChange = saveGame.Bet;
                        saveGame.Status = 0;
                    }
                    else
                    {
                        Console.WriteLine("You lost: " + saveGame.Bet + " $.");
                        _balans -= saveGame.Bet;
                        saveGame.BalanceChange = saveGame.Bet;
                        saveGame.Status = 1;
                    }
                }
                saveGame.BalansEndGame = _balans;
                _logDataBase.AddEntryDataBase(saveGame);

               CasinoUtils.PrintContinueGame();
               choice = CasinoUtils.ReadingNumberRange(1, 2);
            }

        }
        public void Dice()
        {
            int choice = 1;
            while (choice == 1)
            {
                GameCasino save = new GameCasino(_nameGamer, 2);
                Console.WriteLine("Make your bid from 0 to " + _balans + ":");
                save.Bet = CasinoUtils.ReadingNumberRange(0, _balans);
                save.BalanceChange = save.Bet;

                Console.WriteLine("At what outcome you bet?");
                Console.WriteLine("Enter a number from 2 to 12.");
                save.SummGamer = CasinoUtils.ReadingNumberRange(2, 12).ToString();

                Random rand = new Random();
                save.SummCasino = rand.Next(2, 12);

                Console.WriteLine("Dropped: " + save.SummCasino + ".");
                if (Convert.ToInt32(save.SummGamer) == save.SummCasino)
                {

                    Console.WriteLine("You won: " + save.Bet + " $.");
                    _balans += save.Bet;
                    save.Status = 0;
                }
                else
                {
                    Console.WriteLine("You lost: " + save.Bet + " $.");
                    _balans -= save.Bet;
                    save.Status = 1;
                }
                save.BalansEndGame = _balans;
                _logDataBase.AddEntryDataBase(save);

                CasinoUtils.PrintContinueGame();
                choice = CasinoUtils.ReadingNumberRange(1, 2);
            }
        }
        public void BlackJack()
        {
            int choice = 1;
            while (choice == 1)
            {
                int[] cart = CasinoUtils.GetPackPlayingCards();

                GameCasino save = new GameCasino(_nameGamer, 0);
                Console.WriteLine("Make your bid from 0 to " + _balans + ":");
                save.BalanceChange = save.Bet = CasinoUtils.ReadingNumberRange(0, _balans);

                int summGamer =  0;
                int summCasino = 0;
                int j = 1;

                summGamer += CasinoUtils.CardToPoints(cart[j]) + CasinoUtils.CardToPoints(cart[j + 1]);
                j += 2;
                summCasino += CasinoUtils.CardToPoints(cart[j]) + CasinoUtils.CardToPoints(cart[j + 1]);
                j += 2;

                while (true)
                {
                    Console.WriteLine("Your total score:" + summGamer + ". Do you want to take another card?");
                    Console.WriteLine("1 - Yes");
                    Console.WriteLine("2 - No");
                    int step = CasinoUtils.ReadingNumberRange(1, 2);
                    if (step == 2)
                    {
                        break;
                    }
                    else
                    {
                        summGamer += CasinoUtils.CardToPoints(cart[j]);
                        ++j;
                        if (summCasino <= 14)
                        {
                            summCasino += CasinoUtils.CardToPoints(cart[j]);
                            ++j;
                        }

                        if (summCasino > 21 || summGamer > 21)
                        {
                            break;
                        }
                    }
                }

                Console.WriteLine("Your total score:" + save.SummGamer);
                Console.WriteLine("Casino total score:" + save.SummCasino);
                if (summCasino > 21)
                {
                    Console.WriteLine("Bust the casino! You won:" + save.Bet + "$!");
                    _balans += save.Bet;
                    save.Status = 0;
                }
                else if (summGamer > 21)
                {
                    Console.WriteLine("Bust you! You lost:" + save.Bet + "$!");
                    _balans -= save.Bet;
                    save.Status = 1;
                }
                else if (summGamer > summCasino)
                {   
                    Console.WriteLine("You won:" + save.Bet + ":)");
                    _balans += save.Bet;
                    save.Status = 0;
                }
                else
                {
                    Console.WriteLine("You lost:" + save.Bet + "!");
                    _balans -= save.Bet;
                    save.Status = 1;
                }
                save.SummCasino    =  summCasino;
                save.SummGamer     =  summGamer.ToString();
                save.BalansEndGame =  _balans;
                _logDataBase.AddEntryDataBase(save);

                CasinoUtils.PrintContinueGame();
                choice = CasinoUtils.ReadingNumberRange(1, 2);
            }

        }
        public void PrintLogs()
        {
            Console.WriteLine("Your balance:" + _balans + ".");
            var log = _logDataBase.GetAllPlayerGames(_nameGamer);
            foreach (var i in log)
            {
                    Console.WriteLine(i);
            }
        }

        public void NewBalanse()
        {
            Console.WriteLine("Your balance:" + _balans);
            Console.WriteLine("How much money you want to put into the account?");
            Console.WriteLine("Enter a number from 0 to 100000");
            int x = CasinoUtils.ReadingNumberRange(0, 100000);
            _balans += x;
            _logDataBase.AddDepoisite(_nameGamer,_balans);
            Console.WriteLine("Your balance:" + _balans);
        }

        private int      _balans;
        private string   _nameGamer;
        private DataBase _logDataBase;

    }
}