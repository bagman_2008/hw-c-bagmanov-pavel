﻿using System;
using System.Collections.Generic;
using System.IO;
using CasinoAnalysis;

namespace HomeWork1
{
    class DataBase
    {
        public DataBase(string nameFile)
        {
            FileNameLog = nameFile;
            using (File.Open(FileNameLog,FileMode.OpenOrCreate))
            {
                
            }
            userDictonary = new Dictionary<string, User>();
            using (StreamReader stream = File.OpenText(FileNameLog))
            {
                string str;
                while ((str = stream.ReadLine()) != null)
                {
                    if (str.Contains("DEPOSITE"))
                    {
                        User newUser = DataBaseUtils.ConvertStringToUser(str);
                        if (userDictonary.ContainsKey(newUser.Name))
                        {
                            userDictonary[newUser.Name] = newUser;
                        }
                        else
                        {
                            userDictonary.Add(newUser.Name, newUser);
                        }
                    }
                }
            }
        }

        //HomeWork1
        public void AddEntryDataBase (GameCasino result)
        {
            using (StreamWriter stream = File.AppendText(FileNameLog))
            {
                string commonStr = DataBaseUtils.ConvertDateToString(result.Date) + " " + result.NameUser;

                string strDeposite = commonStr + " " + "DEPOSITE:" + result.BalansEndGame;
                stream.WriteLine(strDeposite);

                string strBet      = commonStr + " "+ result.GameCode + " " + "BET:" + result.Bet;
                stream.WriteLine(strBet);

                string strGame     = commonStr +" "+ result.GameCode + " " + result.Status + " " + 
                                     result.BalanceChange    + " " + result.SummGamer      + " " + 
                                     result.SummCasino;
                stream.WriteLine(strGame);
            }

            userDictonary[result.NameUser].Balabce = result.BalansEndGame;

        }
        public bool SearchGamer      (string nameGamer)
        {
            if (userDictonary.ContainsKey(nameGamer))
            {
                return true;
            }
            return false;
        }
        public int  GetBalanseGamer  (string nameGamer)
        {
            if (userDictonary.ContainsKey(nameGamer))
            {
                return userDictonary[nameGamer].Balabce;
            }
            return 0;
        }
        public void AddNewGamer      (string nameUser)
        {
            if (SearchGamer(nameUser)) return;

            using (StreamWriter stream = File.AppendText(FileNameLog))
            {
                DateTime saveUtcNow = DateTime.UtcNow;
                string strDeposite = DataBaseUtils.ConvertDateToString(saveUtcNow) + " " + nameUser + " " + "DEPOSITE:0";
                stream.WriteLine(strDeposite);
            }
            User newUser = new User();
            newUser.Name = nameUser;
            newUser.Balabce = 0;
            userDictonary.Add(newUser.Name, newUser);
        }
        public void AddDepoisite     (string nameUser, int balanse)
        {
            if (SearchGamer(nameUser))
            {
                balanse += GetBalanseGamer(nameUser);
            }
            using (StreamWriter stream = File.AppendText(FileNameLog))
            {
                DateTime saveUtcNow = DateTime.UtcNow;
                string strDeposite =  DataBaseUtils.ConvertDateToString(saveUtcNow) + " " + nameUser + " " + "DEPOSITE:" + balanse;
                stream.WriteLine(strDeposite);
            }
            userDictonary[nameUser].Balabce = balanse;
        }

        public List<GameCasino> GetAllPlayerGames(string name)
        {
            List<GameCasino> listGameCasino = new List<GameCasino>();
            using (StreamReader stream = File.OpenText(FileNameLog))
            {
                string str;
                while ((str = stream.ReadLine()) != null)
                {
                    if (str.Contains(name) && !str.Contains("BET") && !str.Contains("DEPOSITE"))
                    {
                        GameCasino x = new GameCasino(str);
                        listGameCasino.Add(x);
                    }
                }
            }
            return listGameCasino;
        } 


        //HomeWork 2
        public List<Bet>                GetAllBets()
        {
            List<Bet> listBets = new List<Bet>();
            using (StreamReader stream = File.OpenText(FileNameLog))
            {
                string str;
                while ((str = stream.ReadLine()) != null)
                {
                    if (str.Contains("BET"))
                    {
                        Bet newBet = DataBaseUtils.ConvertStringToBet(str,this);
                        listBets.Add(newBet);
                    }
                }
            }
            return listBets;
        }
        public List<Deposite>           GetAllDeposites()
        {
            List<Deposite> listDeposites = new List<Deposite>();
            using (StreamReader stream = File.OpenText(FileNameLog))
            {
                string str;
                while ((str = stream.ReadLine()) != null)
                {
                    if (str.Contains("DEPOSITE"))
                    {
                        Deposite newDeposite = DataBaseUtils.ConvertStringToDeposite(str,this);
                        listDeposites.Add(newDeposite);
                    }
                }
            }
            return listDeposites;
        }
        public List<GameResults>        GetAllGame()
        {
            List<GameResults> listGame = new List<GameResults>();
            using (StreamReader stream = File.OpenText(FileNameLog))
            {
                string str;
                while ((str = stream.ReadLine()) != null)
                {
                    if (!str.Contains("DEPOSITE") && !str.Contains("BET"))
                    {
                        GameResults newGame = DataBaseUtils.ConvertStringToGameResults(str, this);
                        listGame.Add(newGame);
                    }
                }
            }
            return listGame;
        }
        public Dictionary<string, User> GetAllUser()
        {
            return userDictonary;
        }

        public User GetUser            (string name)
        {
            return userDictonary[name];
        }

        public string FileNameLog { get; private set; }
        private Dictionary<string, User> userDictonary;
    }
}
