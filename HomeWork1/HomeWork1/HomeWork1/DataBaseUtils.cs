﻿using System;
using CasinoAnalysis;

namespace HomeWork1
{
    class DataBaseUtils
    {
        public static string   ConvertDateToString(DateTime input)
        {
            DateTimeOffset dateOffset = input;
            string str = "[" + dateOffset.ToString("O").Replace(" ", "|") + "]";

            return str;
        }
        public static DateTime ConvertStringToDate(string input)
        {
            string str = input.Replace("[", string.Empty).Replace("]", string.Empty);
            DateTimeOffset dateOffset = DateTimeOffset.Parse(str);
            DateTime output = dateOffset.UtcDateTime;
            return output;
        }

       
        public static User        ConvertStringToUser(string input)
        {
            string[] prsStr = input.Split(' ');
            User newUser = new User();

            newUser.Name = prsStr[1];
            newUser.Balabce = Convert.ToInt32((prsStr[2].Split(':'))[1]);

            return newUser;
        }
        public static Bet         ConvertStringToBet (string input, DataBase dataBase)
        {
            string[] arrayStr = input.Split(' ');
            Bet newBet = new Bet();
            newBet.Date = ConvertStringToDate(arrayStr[0]);
            newBet.User = dataBase.GetUser(arrayStr[1]);

            int x = Convert.ToInt32(arrayStr[2]);
            if (x == 0)
            {
                newBet.GameCode = GameCode.BJ;
            }
            else if (x == 1)
            {
                newBet.GameCode = GameCode.Roulet;
            }
            else
            {
                newBet.GameCode = GameCode.Dice;
            }

            arrayStr = arrayStr[3].Split(':');
            newBet.Value = Convert.ToInt32(arrayStr[1]);
            return newBet;
        }

        public static Deposite    ConvertStringToDeposite   (string input, DataBase dataBase)
        {
            Deposite newDeposite = new Deposite();
            string[] arrayStr = input.Split(' ');
            newDeposite.Date = ConvertStringToDate(arrayStr[0]);
            newDeposite.User = dataBase.GetUser(arrayStr[1]);
            newDeposite.Value = Convert.ToInt32((arrayStr[2].Split(':'))[1]);
            return newDeposite;
        }
        public static GameResults ConvertStringToGameResults(string input, DataBase dataBase)
        {
            GameCasino crt = new GameCasino(input);
            if (crt.GameCode == 1)
            {
                RouletGameResults newRouletGameResults = new RouletGameResults(crt.Date, dataBase.GetUser(crt.NameUser),
                                                                               crt.Status == 0 ? GameResultStatus.Win : GameResultStatus.Loose,
                                                                               crt.BalanceChange, crt.SummGamer, crt.SummCasino);
                return newRouletGameResults;
            }
            else if (crt.GameCode == 2)
            {
                DiceGameResults newDiceGameResults = new DiceGameResults(crt.Date, dataBase.GetUser(crt.NameUser),
                                                                         crt.Status == 0 ? GameResultStatus.Win : GameResultStatus.Loose,
                                                                         crt.BalanceChange, Convert.ToInt32(crt.SummGamer), crt.SummCasino);
                return newDiceGameResults;
            }
            else
            {
                BlackJackGameResults newBlackJackGameResults = new BlackJackGameResults(crt.Date, dataBase.GetUser(crt.NameUser),
                                                                                        crt.Status == 0 ? GameResultStatus.Win : GameResultStatus.Loose,
                                                                                        crt.BalanceChange, Convert.ToInt32(crt.SummGamer), crt.SummCasino);
                return newBlackJackGameResults;
            }
        }
    }

}
